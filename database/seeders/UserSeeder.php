<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insertOrIgnore([
          'name' => 'Administrator',
          'email' => 'admin@admin.com',
          'password' => password_hash('12345', PASSWORD_BCRYPT)
        ]);
    }
}
