<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AuthClient extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->updateOrInsert([
          'id' => 1
        ],[
          'name' => 'Auth Client',
          'secret' => '9eL4KMPYexuejKNqjcIuEBFFw25M4KsyOcdE90FC',
          'provider' => 'users',
          'redirect' => 'http://localhost',
          'personal_access_client' => 0,
          'password_client' => 1,
          'revoked' => 0
        ]);
    }
}
