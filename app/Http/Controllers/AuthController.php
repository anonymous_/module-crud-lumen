<?php

namespace App\Http\Controllers;

use App\Helper\AuthLogin;
use Auth;
use App\Helper\BaseCrud as crud;
use Illuminate\Http\Request;
use App\Helper\Validate;

class AuthController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function checkUser(){
      $item = AuthLogin::checkUserLogin("user");
      return $item;
    }

    
}
