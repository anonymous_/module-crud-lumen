<?php

namespace App\Http\Controllers;
use Auth;
use App\Helper\BaseCrud as crud;
use App\Models\User;
use Illuminate\Http\Request;
use App\Helper\Validate;

class UserController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get(){
      $item = crud::showAll(new User);
      return $item;
    }

    public function getWhere($id){
      $item = crud::showOne(new User, $id);
      return $item;
    }

    public function create(Request $params){
      $params = $params->all();

      $v_required   = Validate::validationRequired($params, ['name', 'email', 'password']);
      $v_uniq       = Validate::validationUniq(new User, 'email', $params['email']);
      if(!$v_required['success']){
        return response()->json($v_required, 411);
      }

      if(!$v_uniq['success']){
        return response()->json($v_uniq, 400);
      }

      if(isset($params['password'])){
        $params['password'] = password_hash($params['password'], PASSWORD_BCRYPT);
      }
      $item = crud::insert(new User, $params);
      return $item;
    }

    public function update($id, Request $params){
      $params = $params->all();
      if(isset($params['password'])){
        $params['password'] = password_hash($params['password'], PASSWORD_BCRYPT);
      }
      $item = crud::update(new User, $params, $id);
      return $item;
    }

    public function updateWhere(Request $params){
      $where  = $params->where;
      $params = $params->data;
      if(isset($params['password'])){
        $params['password'] = password_hash($params['password'], PASSWORD_BCRYPT);
      }
      $item = crud::updateWhere(new User, $params, $where);
      return $item;
    }

    public function delete($id){
      $item = crud::delete(new User, $id);
      return $item;
    }

    public function deleteWhere(Request $params){
      $item = crud::deleteWhere(new User, $params->where);
      return $item;
    }
}
