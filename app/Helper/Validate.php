<?php
namespace App\Helper;

class Validate{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	public static function validationRequired($params, $validate){
		$error = array();
		for($i = 0; $i<count($validate); $i++){
			if(!isset($params[$validate[$i]])){
				array_push($error, $validate[$i]." is required");
			}
		}
		if(count($error) > 0){
			return ['success' => false, 'message' => $error];
		}
		return ['success' => true];
	}

	public static function validationUniq($model, $fk, $value){
		if($model::where($fk, $value)->count() > 0){
			return ['success' => false, 'message' => $fk.' sudah terdaftar'];
		}
		return ['success' => true];
	}
}