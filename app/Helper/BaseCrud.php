<?php
namespace App\Helper;

use Illuminate\Database\QueryException;

class BaseCrud{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	public static function showAll($model){
		return response()->json(['success' => true, 'data' =>$model::all()], 200);
	}

	public static function showOne($model, $id){
		return response()->json(['success' => true, 'data' =>$model::find($id)], 200);
	}

	public static function insert($model, $data){
		try { 
			$model::create($data);
		} catch(QueryException $ex){ 
			return response()->json(['success' => false, 'message' => $ex->getMessage()], 400);
		}
		return response()->json(['success' => true, 'message' => 'insert berhasil'], 201);
	}

	public static function update($model, $data, $id){
		try {
			if(!$model::where('id', $id)->count()){
				return response()->json(['success' => false, 'message' => 'Data tidak di temukan'], 400);
			}
			
			$model::findOrFail($id)->update($data);
		} catch(QueryException $ex){ 
			return response()->json(['success' => false, 'message' => $ex->getMessage()], 400);
		}
		return response()->json(['success' => true, 'message' => 'update berhasil'], 201);
	}

	public static function updateWhere($model, $data, $where){
		try {
			if(!$model::where($where)->count()){
				return response()->json(['success' => false, 'message' => 'Data tidak di temukan'], 400);
			}

			$model::where($where)->update($data);
		} catch(QueryException $ex){ 
			return response()->json(['success' => false, 'message' => $ex->getMessage()], 400);
		}
		return response()->json(['success' => true, 'message' => 'update berhasil'], 201);
	}

	public static function delete($model, $id){
		try {
			if(!$model::where('id', $id)->count()){
				return response()->json(['success' => false, 'message' => 'Data tidak di temukan'], 400);
			}

			$model::findOrFail($id)->delete();			
		} catch(QueryException $ex){ 
			return response()->json(['success' => false, 'message' => $ex->getMessage()], 400);
		}
		return response()->json(['success' => true, 'message' => 'delete berhasil'], 200);
	}

	public static function deleteWhere($model, $where){
		try {
			if(!$model::where($where)->count()){
				return response()->json(['success' => false, 'message' => 'Data tidak di temukan'], 400);
			}

			$model::where($where)->delete();
			
		} catch(QueryException $ex){ 
			return response()->json(['success' => false, 'message' => $ex->getMessage()], 400);
		}
		return response()->json(['success' => true, 'message' => 'delete berhasil'], 200);
	}
}
