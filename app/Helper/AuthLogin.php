<?php
namespace App\Helper;
use Illuminate\Support\Facades\Auth;

class AuthLogin{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	public static function checkUserLogin($guard){
		$data = Auth::guard($guard)->user();
		return ['success' => true, 'data' => $data];
	}
}