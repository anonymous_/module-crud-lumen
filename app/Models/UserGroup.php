<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class UserGroup extends Model
{
    //
    protected $fillable = [
        'name', 'slug'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}