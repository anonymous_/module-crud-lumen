<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('auth/login','AuthController@prosesLogin');
/* Route with auth middleware */
$router->group([
  'middleware' => 'auth'
],function() use ($router){
  /* User Routes */
  $router->group([
    'prefix' => 'user'
  ],function() use ($router){
    $router->get('','UserController@get');
    $router->get('{id}','UserController@getWhere');
    $router->post('','UserController@create');
    $router->put('{id}','UserController@update');
    $router->put('','UserController@updateWhere');
    $router->delete('{id}','UserController@delete');
    $router->delete('','UserController@deleteWhere');
  });

  $router->group([
    'prefix' => 'auth'
  ],function() use ($router){
    $router->get('','AuthController@checkUser');
  });

});