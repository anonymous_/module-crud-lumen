<?php

class AuthTest extends TestCase{
	
	public function testLogin(){
		$params = [
			"grant_type" 	=> "password",
			"client_id" 	=> 1,
			"client_secret" => "9eL4KMPYexuejKNqjcIuEBFFw25M4KsyOcdE90FC",
			"username" 		=> "test@gmail.com",
			"password" 		=> "12"
		];
		$this->post("oauth/token", $params);
		$this->seeStatusCode(200);
		$this->seeJsonStructure([
			'token_type',
			'expires_in',
			'access_token',
			'refresh_token'
		]);
	}

	public function testCheckUser(){
		$header = [
			"Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZGJkNDcwN2VmN2Q2OWJhYWMxYzk0MTQwZjQwYjkwODkyN2M2YTk0YjcxNjgwMGZjZmQ0ZGI2ODQ3NTQxNDZmYWM3NmRjNmQ1MTMzMTVkY2MiLCJpYXQiOjE2MDMzMzYwNjgsIm5iZiI6MTYwMzMzNjA2OCwiZXhwIjoxNjM0ODcyMDY3LCJzdWIiOiIxOSIsInNjb3BlcyI6W119.sYUIccUMWJD6YLWVWbt1qNvlbwL2XgdfmgB6vO1sYHwTs-ay9C87tg9qOEsADxp7bG9qk4aavU9kHLCKlxh53p2HjlR7waxBU061ssQpZNnYBN8gvQC_guIJoDnk1KcwoAYtoEzERvpNrA7EFCB4R0Ln79oqus4fTcvga3_sCHRBAEvMIq4Ejfd8wu3ffZfOP2YAwcOl0llvl6vmLMwMZhMC-PQugNvh8scqm5ynnbHWsJtT9JKOhEa0AQnnZJKnZ6RwGY6UjYqIeo234eLG_W_102iKXmiWVnE9WOvthx_4wLfC9Blp8kcOKl2VV_DZD2sN90vS3QqaJObNT3VnPKkbRM_7-Fx_lNGtwSoMDP0MQyC4-hcof5jclomjVEZfMqTaGHYEfBCcZpGwdYx-UoNRFJib0evj0pWGcSlKDsln6Hc8a1HXfZUZBfDM31QTWbua5NBFFQ2t7wkOa_47ytXENlqcSrjErmZ1ezd50WgS8zoQj-cBKiLf86zeK2uE407qsTqQAHO6DqJAtwejetYc5yifAjg9H9TjU7C45txKZfjaEBoj9YDek5YWOBHPGZb3Hxjo8p_Ba1yO1Axhwrk7tk6h6vXg2aXtcWYmo8YD34s2yE6hm9EXHm3Ns-KtiGBDzofooiXJSUjidgXDwwa7AQKV0dmhYvV3qqU_2aE"];
		$this->get("auth", $header);
		$this->seeStatusCode(401);
		$this->seeJsonStructure([
			'success',
			'message'
		]);
	}
}